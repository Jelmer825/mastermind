# README #

### What is this repository for? ###

* Mastermind
* Algorithms
* Source reference

### Mastermind ###

* Mastermind or Master Mind is a code-breaking game for two players. The modern game with pegs was invented in 1970 by Mordecai Meirowitz, an Israeli postmaster and telecommunications expert. It resembles an earlier pencil and paper game called Bulls and Cows that may date back a century.

### Algorithms ###

* Simple Strategy: The simple strategy has an avarage of 5.7 turns before it guesses the correct answer. 
* Worst case: The Worst case strategy has an avarage of 4.9 turns before it guesses the correct answer. 
* Expected size strategy:The Expected size strategy has an avarage of 4.1 turns before it guesses the correct answer.

* My own algorithm is doing the following:.
* Loop through all possible guesses, compare the answers to the power of two and make a new list out of that. This way you get a wider span of options left than with the simple algorithm.
* Conclusion: This algorithm has an avarage guesses of 7.2 so it is NOT faster than the other 3 algorithms above. 

### Source reference ###

* I made my base game with some code of:
https://www.askpython.com/python/examples/create-mastermind-game-in-python

* For the algorithms simple, worst case and expected size i used this article from the university of Groningen:
https://canvas.hu.nl/courses/22629/files/1520303/download?wrap=1

* The python libs i used:
* Itertools: https://docs.python.org/3/library/itertools.html
* Random: https://docs.python.org/3/library/random.html
* Time: https://docs.python.org/3/library/time.html