import random
import itertools
class Bot():

    # All guesses the AI has made.
    guessList = []

    # All the feedback the ai received
    feedbackList = []

    # All the possible guesses the AI can make
    guessOptions = []

    def botGuess(self, feedback, turn, algo):
        """
            The bot makes a guess
        """

        # Add feedback to the list
        self.feedbackList.append(feedback)

        # Create all possible options
        self.makeOptions(turn, feedback)

        if algo == 1:
            # Do first element from options as guess
            guess = self.getGuess()
        elif algo == 2:
            # Worst case
            guess = self.worstCase(turn, feedback)
        elif algo == 3:
            # Expected size
            guess = self.expectedSize(turn)
        elif algo == 4:
            guess = self.ownSolution(turn, feedback)
        else:
            guess = random.choice(self.guessOptions)

        # Add next guess to list
        self.guessList.append(guess)

        return guess

    def getGuess(self):
        """
            Generate guess (Simple strategy)
        """
        nextGuess = self.guessOptions[0]
        return nextGuess

    def makeOptions(self, turn, feedback):
        """
            Make a list of all possible guesses (Simple strategy)
        """

        if turn == 0:
            # Generate all possible options
            self.allOptions()
        else:
            self.guessOptions = self.makeNewList(self.guessList[-1], feedback)

    def allOptions(self):
        """
            Generate first set of options
        """

        # All possible inputs
        possibleNums = [1, 2, 3, 4, 5, 6]

        # Generate a list of all possible combinations
        allOptionsTuple = list(itertools.product(possibleNums, repeat=4))

        # Convert all tuples to lists for later use
        allOptionsList = [list(elem) for elem in allOptionsTuple if elem not in self.guessList]

        # Set all guess options
        self.guessOptions = allOptionsList

        return allOptionsList

    def makeNewList(self, lastGuess, feedback):
        """
            Creates new list with possible options
        """
        newList = []
        for guess in self.guessOptions:
            if self.validateGuess(lastGuess, guess) == feedback:
                newList.append(guess)
        return newList

    def validateGuess(self, guess, secret):
        """
            Validate 2 guesses on POSSIBLE feedback
        """
        # Set feedback
        feedback = [0, 0]

        # Copy secret code to modify it without consequence
        copyPass = secret.copy()

        # Position at feedback board
        pos = 0

        for x in guess:
            if x in copyPass:
                if guess.index(x) == secret.index(x):
                    feedback[0] += 1
                else:
                    feedback[1] += 1
                pos += 1

                # Remove color from copyPass to prevent redundancy.
                copyPass.remove(x)
        return feedback

    def expectedSize(self, turn):
        """
            Expected size strategy
        """
        # Set best guess for first turn
        if turn == 0:
            return [1, 1, 2, 3]

        # List of all possible answers
        possibleAnswers = [[0, 0], [0, 1], [0, 2], [0, 3], [0, 4], [1, 0], [1, 1], [1, 2], [1, 3], [2, 0], [2, 1], [2, 2], [3, 0], [4, 0]]

        # Copy all options left
        tempOptions = self.guessOptions.copy()

        # Set best option
        bestOption = []
        for option in tempOptions:
            # Expected partition size
            size = 0

            if not bestOption:
                bestOption.append([[option], 9999])

            # Loop through all possible answers
            for answer in possibleAnswers:
                # Calculate amount of options left
                amountOfOptions = len(self.makeNewList(option, answer))
                expectedSize = (amountOfOptions ** 2) / 1296
                size += expectedSize

            # If new guess is better than last best guess
            if size < bestOption[0][1]:
                bestOption = [[option, size]]

        # Return the best option
        return bestOption[0][0]

    def worstCase(self, turn, feedback=None):
        """
            Worst case strategy
        """
        # Set best guess for first turn:
        if turn == 0:
            return [1, 1, 2, 2]

        # Set feedback for first guess
        if feedback is None:
            feedback = [0, 0]

        # Copy all options left
        tempOptions = self.guessOptions.copy()

        # List for best options
        bestOptions = []

        # Check how many options there are left for each option left
        for option in tempOptions:
            optionsLeft = len(self.makeNewList(option, feedback))
            bestOptions.append([option, optionsLeft])

        # Set the best possible option
        bestOption = []
        for option in bestOptions:
            if bestOption:
                if option[1] < bestOption[1] and option[1] != 0:
                    bestOption = option
            else:
                bestOption = option

        return bestOption[0]

    def ownSolution(self, turn, feedback):
        """
            My own solution
            This function makes a wider span of options than the simple algorithm
        """
        # Set 'best' guess for first turn
        if turn == 0:
            return [1, 2, 3, 4]

        # To the power of feedback!
        feedbackPower = feedback[0] ** feedback[1]

        # Make new list
        newList = []

        # Loop through all guesse
        for guess in self.guessOptions:
            guessJong = self.validateGuess(self.guessList[-1], guess)
            if guessJong[0] ** guessJong[1] == feedbackPower:
                newList.append(guess)

        # Return random guess
        return random.choice(newList)

